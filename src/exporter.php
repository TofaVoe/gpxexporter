<?php
/**
 * Class exporter
 * @author Kryštof Košut
 */

use phpGPX\phpGPX;
use phpGPX\Models\GpxFile;
use phpGPX\Models\Link;
use phpGPX\Models\Metadata;
use phpGPX\Models\Point;
use phpGPX\Models\Segment;
use phpGPX\Models\Track;
use ZipStream\Option\Archive;

require_once 'vendor/autoload.php';

class exporter
{
    /**
     * @var array
     */
    private $data = array();

    /**
     * @var string gps or json
     */
    private $dataFormat = "gpx";

    /**
     * @var array
     */
    private $dataFormatAvailable = array("json", "gpx");

    /**
     * @var array
     */
    private $errorLog = array();

    /**
     * @var string
     */
    private $filename = "";

    /**
     * @var bool
     */
    private $error = false;

    /**
     * @var string
     */
    public $longitude = "longitude";

    /**
     * @var string
     */
    public $latitude = "latitude";

    /**
     * @var string
     */
    public $elevation = "elevation";

    /**
     * @var string
     */
    public $vehicleName = "";

    /**
     * @var string
     */
    public $trackName = "";

    /**
     * @var string
     */
    public $time = "time";

    /**
     * @param array $data
     */
    public function setData($data): void
    {
        if (is_array($data)){
            $this->data = $data;
            $this->transformData();
        } else {
            $this->error = "Data not array";
        }
    }

    private function transformData(): array
    {
        if (count ($this->data) == 0){
            $this->error = "No data";
        }

        $newData = array();
        foreach ($this->data as $index => $datum) {
            $newData[] = [
                'longitude' => $datum[$this->longitude],
                'latitude' => $datum[$this->latitude],
                'elevation' => $datum[$this->elevation],
                'time' => $datum[$this->time]
            ];
        }
        return $newData;
    }

    public function process()
    {
        $data = $this->transformData();
        if ($this->error === true){
            return false;
        }

        $this->setFilename();

        // Creating sample link object for metadata
        $link 							= new Link();
        $link->href 					= "https://sibyx.github.io/phpgpx";
        $link->text 					= 'phpGPX Docs';

        // GpxFile contains data and handles serialization of objects
        $gpx_file 						= new GpxFile();

        // Creating sample Metadata object
        $gpx_file->metadata 			= new Metadata();

        // Time attribute is always \DateTime object!
        $gpx_file->metadata->time 		= new \DateTime();

        // Description of GPX file
        $gpx_file->metadata->description = "My pretty awesome GPX file, created using phpGPX library!";

        // Adding link created before to links array of metadata
        // Metadata of GPX file can contain more than one link
        $gpx_file->metadata->links[] 	= $link;

        // Creating track
        $track 							= new Track();

        // Name of track
        $track->name 					= sprintf($this->trackName);

        // Type of data stored in track
        $track->type 					= 'RUN';

        // Source of GPS coordinates
        // TODO: ??
        $track->source 					= sprintf($this->vehicleName);

        // Creating Track segment
        $segment 						= new Segment();

        foreach ($data as $datum)
        {
            try {
                $time = new \DateTime($datum['time']);
            } catch (Exception $exception) {
                echo $exception->getMessage();
                exit(1);
            }
            // Creating trackpoint
            $point 						= new Point(Point::TRACKPOINT);
            $point->latitude 			= $datum['latitude'];
            $point->longitude 			= $datum['longitude'];
            $point->elevation 			= $datum['elevation'];
            $point->time 				= $time;

            $segment->points[] 			= $point;
        }
        // Add segment to segment array of track
        $track->segments[] 				= $segment;
        $track->recalculateStats();
        // Add track to file
        $gpx_file->tracks[] 			= $track;

        // Save
        switch ($this->dataFormat){
            case 'json':
                $gpx_file->save("/var/www/loksys.cz/portal/data/json/".$this->filename.'.json', \phpGPX\phpGPX::JSON_FORMAT);
                $file = "/var/www/loksys.cz/portal/data/json/".$this->filename.'.json';
                break;

            case 'gpx':
                $gpx_file->save("data/xml/".$this->filename.'.gpx', \phpGPX\phpGPX::XML_FORMAT);
                $file = "/var/www/loksys.cz/portal/data/xml/".$this->filename.'.gpx';
                break;

            default:
                $file = "";
                break;
        }

        // if file exists return name of file

        // Direct GPX output to browser

        // header("Content-Type: application/gpx+xml");
        // header("Content-Disposition: attachment; filename=randomname.gpx");

        // return $gpx_file->toXML()->saveXML();
        // return true;
        return $file;
    }

    /**
     * @param string $error
     */
    public function setError($error): void
    {
        $this->errorLog[] = $error;
    }

    /**
    * @return string
    */
    public function getError(): string
    {
        if ($this->error){
            $output = "<p>";
            foreach ($this->errorLog as $index => $item) {
                $output .= $item."<br>".PHP_EOL;
            }
            $output .= "</p>";
        } else {
            $output = "";
        }
        return $output;
    }

    /**
     * @param string $dataFormat
     */
    public function setDataFormat(string $dataFormat): void
    {
        $dataFormat = strtolower($dataFormat);

        if (in_array($dataFormat, $this->dataFormatAvailable)){
            $this->dataFormat = $dataFormat;
        } else {
            $this->setError("bad data format, selecting default gpx");
        }
    }

    /**
     * @return array
     */
    public function getData(): array
    {
        return $this->data;
    }

    private function setFilename(): void
    {
        $name = $this->vehicleName."__".md5(time());
        $this->filename = $name;
    }

}
