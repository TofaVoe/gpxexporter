<?php declare(strict_types=1);

use PHPUnit\Framework\TestCase;

define("HOST", "m193.server4u.cz");
define("USER", "dbmem.loksys");
define("PASSWORD", "xC9a54ywMn");
define("DATABASE", "portal");

// require_once '../vendor/autoload.php';
require_once '/app/src/exporter.php';

final class exporterTest extends TestCase
{
    /**
     * @var mysqli
     */
    private $mysqli;

    private function getData()
    {
        $timeFrom = "";
        $timeTo = "";
        $vehicleId = 13;
    }

    public function testExportData(): void
    {
        $this->setMysqli();
        $query = "SELECT id_data, dataDatetime, lat, lng, alt FROM Data WHERE vehicle_id = 13 LIMIT 2500";
        $r = $this->mysqli->query($query);
        while ( $rec = $r->fetch_assoc() )
        {
            $data[] = $rec;
        }

        $exporter = new \exporter();
        $exporter->latitude = "lat";
        $exporter->longitude = "lng";
        $exporter->time = "dataDatetime";
        $exporter->elevation = "alt";

        $exporter->setData($data);
        $exporter->process();

        $this->assertEquals(1,1);
    }

    private function setMysqli()
    {
        $mysqli = new \mysqli(HOST, USER, PASSWORD, DATABASE);
        $mysqli->set_charset("utf8");
        $this->mysqli = $mysqli;
    }
}
